# Scope Document

## Shooter

The main idea to develop is a shooter game where yourr mission will be to kill every NPC's you found in a selected enviroment.

### Features and Dependencies

1. 3D models of NPC's.
2. Animations for NPC's.
3. Different skyboxes depending of the place.
4. 3D models of different guns.
5. 3D models for different enviroments.

### Game Loop

1. NPC's movement behaviour.
2. Artificial inteligence for NPC's shoot behaviour.
3. Player movement behaviour.
4. Player shoot behaviour.
5. Input behaviour.

### Other Effects

1. Real lighting global ilumination.
2. Different spacial effects sounds.
3. Different particles systems for guns.

## Museum

The main idea to develop is use the last projects of this course and add some improvements to improve the user experience.

### Features and Dependencies

1. 3D models for the enviroment.
2. 360 videos of different places.

### Game Loop

1. Player movement behaviour.
2. Input behaviour.
3. 360 Video player.

### Other Effects

1. Real lighting global ilumination.
2. Different spacial effects sounds.

## EscapeRoom

The main idea to develop escape room where the main objective is complete the hidden tasks and find the key to exit of room.

### Features and Dependencies

1. 3D models for the enviroment.
2. Animations for different props.
3. 3D model for differents tasks.

### Game Loop

1. Player movement behaviour.
2. Game logic behaviours.
5. Input behaviour.

### Other Effects

1. Cartoon lighting global ilumination.
2. Different spacial effects sounds.
3. Different clues in the scene help the player.

Once the ideas are done, i think the best way is to do the Escape Room project.

## EscapeRoom

The main idea to develop escape room where the main objective is complete the hidden tasks and find the key to exit of room.

### Features and Dependencies

1. 3D models for the enviroment.     
It's a lot and hard work to make in some weeks, so i decided to use an asset of the AssetStore.
2. Animations for different props.     
Instead of animate them i will use iTween library.
3. 3D model for differents tasks.     
Instead of make them i will edit them.

### Game Loop

1. Player movement behaviour.    
I will use the Udacity's one.
2. Game logic behaviours.    
It's affordable.
3. Input behaviour.    
It's affordable.

### Other Effects

1. Cartoon lighting global ilumination.    
I will use a mix ilumination.
2. Different spacial effects sounds.    
It's affordable. Each task will have an AudioSource.
3. Different clues in the scene help the player.    
It's affordable. I will edit some assets to make them.