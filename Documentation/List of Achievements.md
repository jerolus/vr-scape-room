# List of Achievements - EscapeRoom

## Fundamentals

1. Scale achievement (100 points)
2. Animation achievement (100 points)
3. Lighting achievement (100 points)
4. Locomotion achievement (100 points)
5. Physics achievement (100 points)

## Completeness

1. Gamification achievement (250 points)
2. AI achievement (250 points)
3. Alternative Storyline achievement (250 points)

## Challenges

1. User Testing achievement (500 points)