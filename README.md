# VR EscapeRoom

This is one of my projects for [Udacity Google VR Nanodegree Program](https://eu.udacity.com/course/vr-developer-nanodegree--nd017). <br>
With this game for VR you will try to find a way to exit from the pirate tavern. You can move around the scene and interact with different elements. <br>
[Link to video](https://youtu.be/aZnR61uBXDY)

## Setup

- [Unity 2018.1.1f1](https://unity3d.com/es/get-unity/download/archive)
- [GVR SDK for Unity v1.170.0](https://github.com/googlevr/gvr-unity-sdk)

## Contact

Name: Jesús María Parral Puebla <br>
Gmail: jesusmaria31@gmail.com <br>
Linkedin: [Profile](https://www.linkedin.com/in/jesusmariaparralpuebla/) <br>
YouTube: [Channel](https://www.youtube.com/channel/UC69skMwVNJglUjmYYsMLivw)

## List of Achievements

[VR-EscapeRoom Achievements](https://gitlab.com/jerolus/vr-scape-room/blob/master/Documentation/List%20of%20Achievements.md)   
[Link to video explanation](https://youtu.be/J0TtxOrhefs)

## Scope Document

[VR-EscapeRoom Scope Document](https://gitlab.com/jerolus/vr-scape-room/blob/master/Documentation/Scope%20Document.md)