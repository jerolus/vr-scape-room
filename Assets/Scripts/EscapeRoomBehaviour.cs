﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EscapeRoomBehaviour : MonoBehaviour
{
	public Button keyButton;
	public Button rightGunButton;
	public Button leftGunButton;
	public AudioSource doorAudioSource;
	public AudioSource boardAudioSource;
	public AudioClip boardSound;
	public GameObject fireButton;
	public GameObject key;
	public GameObject board;
	public GameObject rightGun;
	public GameObject leftGun;
	public Text doorText;
	private int m_counterRings = 0;
	private bool m_rightGunRotated = false;
	private bool m_leftGunRotated = false;
	private bool m_canExit = false;

	public void OnClickRing()
	{
		m_counterRings ++;
		if (m_counterRings == 3)
		{
			fireButton.SetActive(true);
		}
	}

	public void OnClickFire()
	{
		rightGunButton.gameObject.SetActive(true);
		leftGunButton.gameObject.SetActive(true);
	}

	public void OnClickRightGun()
	{
		boardAudioSource.Play();
		m_rightGunRotated = true;
		rightGun.transform.DORotate(new Vector3(-90f, 360f, 90f), 0.8f).OnComplete(delegate
		{
			CheckGuns();
		});
	}

	public void OnClickLeftGun()
	{
		boardAudioSource.Play();
		m_leftGunRotated = true;
		leftGun.transform.DORotate(new Vector3(-90f, 180f, 90f), 0.8f).OnComplete(delegate
		{
			CheckGuns();
		});
	}

	private void CheckGuns()
	{
		if (m_rightGunRotated && m_leftGunRotated)
		{
			boardAudioSource.PlayOneShot(boardSound);
			keyButton.gameObject.SetActive(true);
			leftGunButton.interactable = false;
			rightGunButton.interactable = false;
			keyButton.interactable = true;
			board.transform.DOLocalMoveY(5.1f, 0.8f);
		}
	}

	public void OnClickKey()
	{
		keyButton.interactable = false;
		m_canExit = true;
		key.SetActive(false);
	}

	public void OnClickDoor()
	{
		if (m_canExit)
		{
			doorAudioSource.Play();
			StartCoroutine(ShowText("Congratulations!", true));
		}
		else
		{
			StartCoroutine(ShowText("Need the key", false));
		}
	}

	private IEnumerator ShowText(string textToShow, bool finishGame)
	{
		doorText.text = textToShow;
		yield return new WaitForSeconds(3f);
		doorText.text = "";
		if (finishGame)
		{
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		}
	}
}
